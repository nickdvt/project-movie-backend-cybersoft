const {Router} = require("express");
const { getAll, getCinemasByCineplex } = require("../controllers/cinema.controller");
const cinemaRouter = Router();

cinemaRouter.get("/byCineplex/", getCinemasByCineplex);
cinemaRouter.get("/", getAll);

module.exports = {
    cinemaRouter,
}