const express = require("express");
const {User} = require("../models");
const { logger } = require("../middlewares/log/logger.middleware");
const { checkExist } = require("../middlewares/validations/checkExist.middleware");
const userRouters = express.Router();

const {getListUser, updateUser, removeUser, createUser, getDetailUser, uploadAvatar} = require("./../controllers/user.controllers");
const { authenticate, authorize } = require("../middlewares/auth/verify-token.middleware");
const { uploadImageSingle } = require("../middlewares/uploads/upload-image.middleware");

// upload image
userRouters.post("/upload-avatar",authenticate, uploadImageSingle("avatar"), uploadAvatar)

// Lấy tất cả người dùng
// url: '/api/user/get-list-user'
// method: GET

userRouters.get('/',logger("Tính năng lấy tất cả người dùng") , getListUser)

// Lấy chi tiết người dùng
// url: '/api/user/get-detail-user/:id'
// method: GET
userRouters.get('/:id',logger("Tính năng lấy chi tiết người dùng"), getDetailUser);

// Tạo mới một User
// url: '/api/users/create-user'
// method: POST
// data : {email, name}
userRouters.post('/',authenticate,authorize(["ADMIN"]), createUser);

// XOÁ PHẦN TỬ
userRouters.delete('/:id',checkExist(User), removeUser);

//UPDATE PHẦN TỬ
userRouters.put('/:id', updateUser);

module.exports = {
    userRouters
}