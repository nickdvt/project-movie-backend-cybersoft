const express = require("express");
const {userRouters} = require('./user.routers.js');
const { authRouter } = require('./auth.routers.js');
const { cinemaRouter } = require("./cinema.router.js");
const rootRouter = express.Router();

rootRouter.use("/users",userRouters);
rootRouter.use("/auth",authRouter);
rootRouter.use("/cinemas",cinemaRouter);

module.exports = {
    rootRouter,
}