const express = require("express");
const {rootRouter} = require('./routers/root.routers');
const path = require("path");
const app = express();


// setup static file để truy cập được các file server
const pulicPathDirectory = path.join(__dirname, "./public");
app.use("/public", express.static(pulicPathDirectory));

// setup app sử dụng data dạng JSON
app.use(express.json());
app.use("/api",rootRouter);

app.get('/', (req, res) => {
    res.send('Hello Cybersoft!')
})




const port = 5000;
app.listen(port, () => {
    console.log(`app run on port ${port}`);
});