const jwt = require("jsonwebtoken");

// Kiểm tra xem người dùng đăng nhập hay chưa
const authenticate = (req, res, next) => {
    const token = req.header("token");
    try {
        const secretKey = "taiDoan";
        const decode = jwt.verify(token, secretKey);
        req.tokenDecode = decode
        next();
      } catch(err) {
          res.status(401).send("Bạn chưa đăng nhập!")
      }
};

// Phân quyền người dùng
const authorize = (userTypeArray) => {
    return (req, res, next) => {
        const {tokenDecode} = req;
        const dk = userTypeArray.findIndex(type => type == tokenDecode.role);
        if(dk != -1 ){
            return next();
        }else{
            res.status(403).send({
                message: "Bạn đã đăng nhập, nhưng không có đủ quyền truy cập"
            })
        }
    }
}

module.exports = {
    authenticate,
    authorize
}