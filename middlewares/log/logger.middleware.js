const logger = (message) => (res, req, next)=>{
    console.log(message);
    next();
};

module.exports = {
    logger,
}