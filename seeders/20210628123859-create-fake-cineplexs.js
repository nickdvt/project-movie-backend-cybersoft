'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert(
      'Cineplexes',
      [
        {
          id: 1,
          name: 'BHD',
          logo: 'link hình BHD',
          createdAt: '2021-06-28 19:45:50',
          updatedAt: '2021-06-28 19:45:51'
        },
        {
          id: 2,
          name: 'DDC',
          logo: 'link hình DDC',
          createdAt: '2021-06-28 19:45:50',
          updatedAt: '2021-06-28 19:45:51'
        },
        {
          id: 3,
          name: 'MegaSG',
          logo: 'link hình MegaSG',
          createdAt: '2021-06-28 19:45:50',
          updatedAt: '2021-06-28 19:45:51'
        }
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('cineplexes', null, {});
  }
};
