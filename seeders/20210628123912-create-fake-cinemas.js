'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert(
      'Cinemas',
      [
        {
          id: 1,
          name: 'BHD Star- Bitexco',
          address: 'L3-Bitexco Icon 68, 2 Hải Triều, Q.1',
          image: 'link hình BHD Star- Bitexco',
          cineplexId: 1,
          createdAt: '2021-06-28 19:45:50',
          updatedAt: '2021-06-28 19:45:50'
        },
        {
          id: 2,
          name: 'BHD Hai Bà Trưng',
          address: 'L3-Bitexco Icon 68, 2 Hải Triều, Q.1',
          image: 'link hình BHD  Hai Bà Trưng',
          cineplexId: 1,
          createdAt: '2021-06-28 19:45:50',
          updatedAt: '2021-06-28 19:45:50'
        },
        {
          id: 3,
          name: 'DDC Nguyễn Kim',
          address: 'L3-Bitexco Icon 68, 2 Hải Triều, Q.1',
          image: 'link hình DDC Nguyễn Kim',
          cineplexId: 2,
          createdAt: '2021-06-28 19:45:50',
          updatedAt: '2021-06-28 19:45:50'
        }
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('Cinemas', null, {});
  }
};
