const { User } = require("../models");
const bcryptjs = require("bcryptjs")

const getListUser = async (req, res) => {
    try {
        const userList = await User.findAll();
        res.status(200).send(userList);
    } catch (error) {
        res.status(500).send(error);
    }
};

const getDetailUser = async (req, res) => {
    const { id } = req.params;
    try {
        const userDetail = await User.findByPk(id);
        if (userDetail) {
            res.status(200).send(userDetail);
        } else {
            res.status(404).send("Not Found!");
        }
    } catch (error) {
        res.status(500).send(error)
    }

};

const createUser = async (req, res) => {
    const { name, email, password, phone, age, role } = req.body;
    try {
        // Tạo ra một chuỗi ngẫu nhiên
        const salt = bcryptjs.genSaltSync(10);
        // Mã hoá password + salt
        const hashPassword = bcryptjs.hashSync(password, salt);
        const newUser = await User.create({ name, email, password: hashPassword, phone, age, role });
        res.status(201).send(newUser);
    } catch (error) {
        res.status(500).send(error)
    }

};

const removeUser = async (req, res) => {
    const { id } = req.params;
    try {
        await User.destroy({
            where: {
                id
            }
        });
        res.status(200).send(`Xoá thành công user có id ${id}`)

    } catch (error) {
        res.status(500).send(error)
    }
};

const updateUser = async (req, res) => {
    const { id } = req.params;
    const { name, email, phone, age, role } = req.body;
    try {
        const userUpdate = await User.findByPk(id);
        if (userUpdate) {
            await User.update({ name, email, phone, age, role }, {
                where: {
                    id
                }
            });
            res.status(200).send(`Update thành công user có id ${id}`)
        } else {
            res.status(404).send("Not Found!")
        }
    } catch (error) {
        res.status(500).send(error)
    }
};

const uploadAvatar = async (req, res) => {
    const {file, tokenDecode} = req;
    const urlImage = `http://localhost:4200/${file.path}`;
    try {
        const userDetail = await User.findByPk(tokenDecode.id);
        userDetail.avatar = urlImage;
        await userDetail.save();
        res.status(200).send(userDetail);
    } catch (error) {
        res.status(500).send(error)
    }
}

module.exports = {
    getListUser,
    getDetailUser,
    createUser,
    removeUser,
    updateUser,
    uploadAvatar
}