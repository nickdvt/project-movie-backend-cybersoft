const {User} = require('../models');
const bcryptjs = require("bcryptjs");
const jwt = require('jsonwebtoken');

const signIn = async (req, res) => {
    const {email,password} = req.body;
    try {
        const userLogin = await User.findOne({ where: { email } });
        if(userLogin){
            //so sánh password
            const isAuth =  bcryptjs.compareSync(password, userLogin.password); // này trả về true or false

            if(isAuth){
                const payload = {
                    id: userLogin.id,
                    email: userLogin.email,
                    role: userLogin.role
                }
                const secretKey = "taiDoan"

                const token = jwt.sign(payload, secretKey, {expiresIn: 365 * 24 * 60 * 60})
                res.status(200).send({ message: "Đăng nhập thành công!", token});
            }else{
                res.status(400).send("Mật khẩu không chính xác!")
            }
        }else{
            res.status(404).send("Not Found Email!")
        }
    } catch (error) {
        res.status(500).send(error)
    }
};

const signUp = async (req, res) => {
    const { name, email, password, phone, age} = req.body;
    try {
        // Tạo ra một chuỗi ngẫu nhiên
        const salt = bcryptjs.genSaltSync(10);
        // Mã hoá password + salt
        const hashPassword = bcryptjs.hashSync(password, salt);
        const newUser = await User.create({ name, email, password: hashPassword, phone, age, role : "CLIENT" });
        res.status(201).send(newUser);
    } catch (error) {
        res.status(500).send(error)
    }
};

module.exports = {
    signIn,
    signUp
}