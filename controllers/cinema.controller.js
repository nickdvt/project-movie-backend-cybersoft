const {Cinema, Cineplex, sequelize} = require("../models");

const getAll = async (req, res) => {
   try {
       const cinemaList = await Cinema.findAll({
           include: [
               {
                   model: Cineplex,
               }
           ]
       });
       res.status(200).send(cinemaList);
   } catch (error) {
        res.status(500).send(error);
   }
}

const getCinemasByCineplex = async (req, res) => {
    const {id} = req.query;
    try {
        const [result] = await sequelize.query(`
            select cinemas.name, cinemas.image from cinemas
            inner join cineplexs
            on cineplexs.id = cinemas.cineplexID
            where cineplexs.id = ${id}
        `);
        res.status(200).send(result);
    } catch (error) {
        res.status(500).send(error);
    }
}

module.exports = {
    getAll,
    getCinemasByCineplex
}